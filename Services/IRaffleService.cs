using System.Collections.Generic;
using HadesUW.Entities;
namespace HadesUW.Services
{
    public interface IRaffleService
    {
        IEnumerable<Raffle> GetList();
        Raffle GetById(int id);
        Raffle Save(string title,string hostId, string prizeUrl, string description, string prize,int cost, int maxTickets, bool freeShip, bool benefic, string countries, string countriesRestricted, RaffleState status);
        //Return true successful
        bool Delete(int id);
    }
}