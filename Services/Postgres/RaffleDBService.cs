using HadesUW.Models;
using HadesUW.Entities;
using System.Linq;
using System.Collections.Generic;

namespace HadesUW.Services.Postgres
{
    public class RaffleDBService: IRaffleService
    {
        private readonly HadesUWContext context;

        public RaffleDBService(HadesUWContext context)
        {
            this.context = context;
        }

        public bool Delete(int id)
        {
            Raffle raffle = context.Raffles.FirstOrDefault(raffle => raffle.Id == id);
            if (raffle!= null)
            {
                context.Raffles.Remove(raffle);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Raffle GetById(int id)
        {
            Raffle raffle = context.Raffles.FirstOrDefault(raffle => raffle.Id == id);
            return raffle;
        }

        public IEnumerable<Raffle> GetList()
        {
            IEnumerable<Raffle> raffles = context.Raffles.ToList();
            return raffles;
        }

        public Raffle Save(string title, string hostId,string prizeUrl, string description, string prize,int cost, int maxTickets, bool freeShip, bool benefic, string countries, string countriesRestricted, RaffleState status)
        {
            Raffle raffle = new Raffle { Title = title, HostId = hostId, PrizeUrl = prizeUrl, Description = description, Prize = prize, MangoesPrice =cost,MaxTickets =maxTickets,FreeShip =freeShip,Benefit =benefic,Countries = countries,CountriesRestricted =countriesRestricted,Status =status};
            context.Raffles.Add(raffle);
            context.SaveChanges();
            return raffle;
        }


    }
}