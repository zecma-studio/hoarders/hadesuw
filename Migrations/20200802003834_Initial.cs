﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace HadesUW.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Raffles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(nullable: true),
                    HostId = table.Column<string>(nullable: true),
                    PrizeUrl = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Prize = table.Column<string>(nullable: true),
                    MangoesPrice = table.Column<int>(nullable: false),
                    MaxTickets = table.Column<int>(nullable: false),
                    FreeShip = table.Column<bool>(nullable: false),
                    Benefit = table.Column<bool>(nullable: false),
                    Countries = table.Column<string>(nullable: true),
                    CountriesRestricted = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    BeneficPercentage = table.Column<float>(nullable: false),
                    OrganizationEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raffles", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Raffles");
        }
    }
}
