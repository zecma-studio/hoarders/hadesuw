namespace HadesUW.Entities
{
    public class Raffle
    {
        public int Id {get;set;}
        public string Title {get;set;}
        //Todo: Verify hostID (should reference Host Entiti?)
        public string HostId {get;set;}        
        public string PrizeUrl {get;set;} //Raffle Price img
        public string Description {get;set;}
        public string Prize {get;set;}
        public int MangoesPrice {get;set;}//
        public int MaxTickets {get;set;}
        public bool FreeShip {get;set;}
        public bool Benefit {get;set;}
        public string Countries {get;set;}
        public string CountriesRestricted {get;set;}
        public RaffleState Status {get;set;}
        public float BeneficPercentage { get; set; }
        public string OrganizationEmail { get; set; }
     }
}