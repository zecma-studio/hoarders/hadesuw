namespace HadesUW.Entities
{
    public enum RaffleState
    {
        Published, GeneratingWinner, ContactingWinner, PriceSent, Finished, Problem, NotPublished
    }
}