using HadesUW.Entities;
using Microsoft.EntityFrameworkCore;

namespace HadesUW.Models
{
    public class HadesUWContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql("Host=localhost;Database=postgres;Username=postgres;Password=docker");
        public DbSet<Raffle> Raffles { get; set; }

    }
}