using HadesUW.Models.Interfaces;
using HadesUW.Services;
using HadesUW.Entities;
using System.Collections.Generic;

namespace HadesUW.Models
{
    public class PublicRaffleModel: IPublicRaffleModel
    {
        private readonly IRaffleService _raffleService;
        public PublicRaffleModel(IRaffleService raffleService)
        {
            this._raffleService = raffleService;   
        }


        public IEnumerable<Raffle> GetList()
        {
            IEnumerable<Raffle> raffles = _raffleService.GetList();
            return raffles;
        }
        public IEnumerable<Raffle> GetList(string countries, string recent){
            IEnumerable<Raffle> raffles = _raffleService.GetList();
            return raffles;
        }
        public Raffle Get(int id){
            Raffle raffle = _raffleService.GetById(id);
            return raffle;
        }
    }
}