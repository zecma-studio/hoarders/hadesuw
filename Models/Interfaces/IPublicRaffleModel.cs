using System.Collections.Generic;
using HadesUW.Entities;
namespace HadesUW.Models.Interfaces
{
    public interface IPublicRaffleModel
    {
        IEnumerable<Raffle> GetList();
        IEnumerable<Raffle> GetList(string countries, string recent);
        Raffle Get(int id);
        
        // Host GetById(int id);
        // Host Save(string title,int hostId, string prizeUrl, string description, string prize,int cost, int maxTickets, bool freeShip, bool benefic, string countries, string countriesRestricted, RaffleState status);
        // //Return true successful
        // bool Delete(int id);
    }
}