using System;
using System.Security.Claims;
using Entities;
using Helpers.Response;
using HadesUW.Entities;
using HadesUW.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace HadesUW.Controllers
{
    [Route("api/raffles")]
    [ApiController]
    public class PublicRaffleController: ControllerBase
    {
        private readonly IPublicRaffleModel _raffleModel;

        public PublicRaffleController(IPublicRaffleModel raffleModel)
        {
            this._raffleModel = raffleModel;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<Response> Get([FromQuery] string countries, [FromQuery] string recent){
             try
            {
                var userId = User;
                var picture = User.Claims.FirstOrDefault(c => c.Type == "https://schemas.quickstarts.com/country")?.Value;
                var raffles = _raffleModel.GetList(countries, recent);
                return new Response { data = raffles, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }


        

    }
}